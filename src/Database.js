
const fs = require('fs');
const shortid = require('shortid');
const path = require("path");

class DataBase {
    constructor(databaseFile, defaultData) {
        this.data = {};
        this.pretty = false;
        this.open(databaseFile, defaultData);
    }

    generateId() {
        return shortid.generate();
    }

    open(databaseFile, defaultData) {
        if (databaseFile) this.file = this.getFilePath(databaseFile);
        try {
            if (fs.existsSync(this.file)) {
                this.data = defaultData !== undefined ? defaultData : {};
                Object.assign(this.data, JSON.parse(fs.readFileSync(this.file)));
            }
            else if (defaultData) {
                this.save(defaultData);
                this.data = defaultData; //JSON.parse(fs.readFileSync(this.file));
            }
            else {
                this.data = undefined;
                return undefined;
            }
        } catch (err) {
            console.error(err)
            this.data = undefined;
            return undefined;
        }
        return this.data;
    }

    close() {
        this.data = null;
        this.file = null;
    }

    deprecate() {
        if (fs.existsSync(this.file)) {
            fs.copyFileSync(this.file, this.file.replace('.json', '_deprecated.json'));
            fs.unlinkSync(this.file);
            this.close();
        }
    }

    save(data, pretty) {
        if (data === undefined) {
            data = this.data;
        }
        if (data === undefined) {
            throw new Error('whoops, seems like you are trying to safe an undefined to a databse file')
        }

        if (pretty || this.pretty) {
            fs.writeFileSync(this.file, JSON.stringify(data, null, 4), (err) => { if (err) console.log(err); });
        }
        else {
            fs.writeFileSync(this.file, JSON.stringify(data), (err) => { if (err) console.log(err); });
        }
    }

    isEmpty() {
        for (var key in this.data) {
            if (this.data.hasOwnProperty(key)) {
                return false;
            }
        }
        return true;
    }

    copy(target) {
        if (target === undefined) {
            target = this.data;
        }
        return JSON.parse(JSON.stringify(target));
    }

    getFilePath(file) {
        if (file) this.file = file;
        return path.resolve(this.file);
    }
}

module.exports = DataBase;