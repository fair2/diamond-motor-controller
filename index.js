const http = require('http');
const express = require('express');
const io = require('socket.io');
const Etherport = require("etherport");
const Firmata = require('firmata-io').Firmata;
const gamepad = require('gamepad');

const DataBase = require('./src/Database.js');
const { setInterval } = require('timers');

const config = new DataBase('config.json', {
    pins: {
        motors: {
            left: 14,
            right: 12,
            center: 3,
            up: 1,
            down: 2,
        }
    },
    controllerDeadzone: 0.15,
    minMotorStrength: 0,
    maxMotorStrength: 1023
});
config.pretty = true;

const animations = new DataBase('animations.json');
animations.pretty = true;

function setConfig(data) {
    Object.assign(config.data, data);
    config.save();
}

function setAnimations(data) {
    Object.assign(animations.data, data);
    animations.save();
}

gamepad.init()

// List the state of all currently attached devices
for (var i = 0, l = gamepad.numDevices(); i < l; i++) {
    console.log(i, gamepad.deviceAtIndex(i).description);
}

setInterval(gamepad.processEvents, 16);
setInterval(gamepad.detectDevices, 500);

const controllerState = {
    leftStick: {
        horizontal: 0,
        vertical: 0
    },
    rightStick: {
        horizontal: 0,
        vertical: 0
    },
    leftTrigger: -1,
    rightTrigger: -1,
};
var controllerStateChanged = true;

gamepad.on("move", function (id, axis, value) {
    switch (axis) {
        case 0:
            controllerState.leftStick.horizontal = value;
            break;
        case 1:
            controllerState.leftStick.vertical = value;
            break;
        case 2:
            controllerState.rightStick.horizontal = value;
            break;
        case 3:
            controllerState.rightStick.vertical = value;
            break;
        case 4:
            controllerState.leftTrigger = value;
            break;
        case 5:
            controllerState.rightTrigger = value;
            break;
        default:
            console.error("Unknown axis:", axis);
            break;
    }
    controllerStateChanged = true;
});

setInterval(() => {
    if (controllerStateChanged) {
        controllerStateChanged = false;
        broadcast('controllerChange', controllerState);
    }
}, 100);

// Listen for button up events on all gamepads
gamepad.on("up", function (id, num) {
    console.log("up", {
        id: id,
        num: num,
    });
});

// Listen for button down events on all gamepads
gamepad.on("down", function (id, num) {
    console.log("down", {
        id: id,
        num: num,
    });
});

const etherPort = new Etherport(3030);
const board = new Firmata(etherPort);

board.on('ready', () => {
    console.log("Board ready");
    board.ready = true;

    board.pinMode(config.data.pins.motors.left, board.MODES.PWM);
    board.pinMode(config.data.pins.motors.right, board.MODES.PWM);
    board.pinMode(config.data.pins.motors.center, board.MODES.PWM);
    board.pinMode(config.data.pins.motors.up, board.MODES.PWM);
    board.pinMode(config.data.pins.motors.down, board.MODES.PWM);

    board.analogWrite(config.data.pins.motors.left, 1023);
    board.analogWrite(config.data.pins.motors.right, 1023);
    board.analogWrite(config.data.pins.motors.center, 1023);
    board.analogWrite(config.data.pins.motors.up, 1023);
    board.analogWrite(config.data.pins.motors.down, 1023);

    board.setSamplingInterval(500);//Set sampling interval high so its not taking up much bandwidth
});


const animationStates = {};
setInterval(() => {
    var currentTime = Date.now();
    handleFrame('left', currentTime);
    handleFrame('right', currentTime);
    handleFrame('up', currentTime);
    handleFrame('down', currentTime);
    updateMotors();
}, 200);

//Update animation
function handleFrame(key, currentTime) {
    var animation = animations.data[key];
    if (animation === undefined) return;
    if (animationStates[key] === undefined) {
        var newObj = {
            startTime: currentTime,
            index: 0,
            frameTime: 0,
            frame: animation[0]
        };
        animationStates[key] = newObj;
    }
    var animationState = animationStates[key];
    if (currentTime - animationState.startTime >= animationState.frameTime + animationState.frame.duration) {
        animationState.frameTime += animationState.frame.duration;
        animationState.index++;
        if (animationState.index >= animation.length) {
            animationState.index = 0;
            animationState.frameTime = 0;
            animationState.startTime = currentTime;
        }
        animationState.frame = animation[animationState.index];
    }
}

const motorsState = {
    left: 0,
    right: 0,
    center: 0,
    up: 0,
    down: 0
};

function updateMotors() {
    var newMotorState = {
        left: 0,
        right: 0,
        center: 0,
        up: 0,
        down: 0
    };

    var controllerKey = 'leftStick';
    //Combine new motor states from animation frames
    for (const key in animationStates) {
        if (
            (key == 'left' && controllerState[controllerKey].horizontal < 0) ||
            (key == 'right' && controllerState[controllerKey].horizontal > 0)
        ) {
            var animationFrame = animationStates[key].frame;
            var multiplier = Math.abs(controllerState[controllerKey].horizontal) > config.data.controllerDeadzone ? Math.abs(controllerState[controllerKey].horizontal) : 0;
            for (const motorKey in animationFrame.motors) {
                newMotorState[motorKey] = Math.max(newMotorState[motorKey], animationFrame.motors[motorKey] * multiplier);
            }
        } else if (
            (key == 'up' && controllerState[controllerKey].vertical > 0) ||
            (key == 'down' && controllerState[controllerKey].vertical < 0)
        ) {
            var animationFrame = animationStates[key].frame;
            var multiplier = Math.abs(controllerState[controllerKey].vertical) > config.data.controllerDeadzone ? Math.abs(controllerState[controllerKey].vertical) : 0;
            for (const motorKey in animationFrame.motors) {
                newMotorState[motorKey] = Math.max(newMotorState[motorKey], animationFrame.motors[motorKey] * multiplier);
            }
        }
    }

    // console.log(motorsState);

    if (board.ready) {
        //Check for changes and write to board if so
        for (const key in newMotorState) {
            if (motorsState[key] != newMotorState[key]) {
                motorsState[key] = newMotorState[key];
                board.analogWrite(config.data.pins.motors[key], Math.round(map(1 - motorsState[key], 0, 1, config.data.minMotorStrength, config.data.maxMotorStrength, true)));
            }
        }
    }

    //Broadcast to browser visualizer
    broadcast('motorsState', motorsState);
}

const app = express();
const server = http.Server(app);
app.use(express.static('./public'));
app.use(express.static('./node_modules'));

const clients = [];
const ioServer = new io(server);
ioServer.on('connect', (socket) => {
    console.log("Client connected");
    clients.push(socket);
    socket.on('disconnect', () => {
        clients.splice(clients.indexOf(socket), 1);
    });
    socket.emit('setConfig', config.data);
    socket.emit('setAnimations', animations.data);

    socket.on('setServerConfig', data => {
        setConfig(data);
        broadcastExcept(socket, 'setConfig', config.data);
    });
    socket.on('setServerAnimations', data => {
        setAnimations(data);
        broadcastExcept(socket, 'setAnimations', animations.data);
    });
    socket.on('reloadAnimations', () => {
        console.log('reloadAnimations');
        animations.open();
    });
});

function broadcast(event, ...args) {
    clients.forEach(socket => {
        socket.emit(event, ...args);
    });
}

function broadcastExcept(except, event, ...args) {
    clients.forEach(socket => {
        if (socket === except) return;
        socket.emit(event, ...args);
    });
}

function map(value, in_min, in_max, out_min, out_max, clampValue) {
    var newValue = (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    if (clampValue) {
        return clamp(newValue, out_min, out_max);
    }
    return newValue;
}

function clamp(val, min, max) {
    return val < min ? min : (val > max ? max : val);
}

server.listen(80);
console.log("Got to http://localhost/ for visualizer and basic config");