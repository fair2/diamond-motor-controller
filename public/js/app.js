
window.addEventListener('load', init);

function init() {
    const socket = io(location.origin + ':80', { transports: ['websocket'], upgrade: false });

    socket.on('connect', function () {
        console.log("Connected");
    });

    window.addEventListener('resize', onResize);

    socket.on('setConfig', data => {
        Object.assign(settings, data);
        createGuiSettings();
        gui.updateDisplay();
    });

    socket.on('setAnimations', data => {
        Object.assign(animations, data);
        createGuiAnimation();
        gui.updateDisplay();
    });

    socket.on('motorsState', data => {
        Object.assign(motorsState, data);
    });

    socket.on('controllerChange', controllerState => {
        Object.assign(state.controller.leftStick, controllerState.leftStick);
        Object.assign(state.controller.rightStick, controllerState.rightStick);
        gui.updateDisplay();
    });

    const settings = {};
    const animations = {};
    const motorsState = {
        left: 0,
        right: 0,
        center: 0,
        up: 0,
        down: 0
    };

    const state = {
        controller: {
            leftStick: {
                horizontal: 0,
                vertical: 0
            },
            rightStick: {
                horizontal: 0,
                vertical: 0
            }
        }
    };

    const commands = {
        reloadAnimations: () => {
            socket.emit('reloadAnimations');
        }
    };

    var gui = new dat.GUI();

    var folderLeftStick = gui.addFolder('LeftStick');
    folderLeftStick.add(state.controller.leftStick, 'horizontal', -1, 1, 0.0001).onChange(onGuiChange);
    folderLeftStick.add(state.controller.leftStick, 'vertical', -1, 1, 0.0001).onChange(onGuiChange);
    // var folderRightStick = gui.addFolder('RightStick');
    // folderRightStick.add(state.controller.rightStick, 'horizontal', -1, 1, 0.0001).onChange(onGuiChange);
    // folderRightStick.add(state.controller.rightStick, 'vertical', -1, 1, 0.0001).onChange(onGuiChange);

    function createGuiSettings() {
        if (state.folderSettings !== undefined) return;

        console.log(settings);

        state.folderSettings = gui.addFolder('Settings');
        state.folderSettings.open();
        state.folderSettings.add(settings, 'controllerDeadzone', 0, 1, 0.01).onChange(onGuiSettingsChange);
        state.folderSettings.add(settings, 'minMotorStrength', 0, 1023, 1).onChange(onGuiSettingsChange);
        state.folderSettings.add(settings, 'maxMotorStrength', 1, 1023, 1).onChange(onGuiSettingsChange);
        state.folderSettings.add(commands, 'reloadAnimations');
    }

    function createGuiAnimation() {
        if (state.folderAnimations) {
            gui.removeFolder(state.folderAnimations);
        }

        state.folderAnimations = gui.addFolder('Animation');
        state.folderAnimations.open();
        for (const key in animations) {
            var animation = animations[key];
            var folderAnimation = state.folderAnimations.addFolder(key);
            animation.forEach((frame, i) => {
                var folderFrame = folderAnimation.addFolder(i);
                folderFrame.open();
                folderFrame.add(frame, 'duration', 1, 5000, 10).onChange(onAnimationChange);
                folderFrame.add(frame.motors, 'left', 0, 1, 0.01).onChange(onAnimationChange);
                folderFrame.add(frame.motors, 'right', 0, 1, 0.01).onChange(onAnimationChange);
                folderFrame.add(frame.motors, 'center', 0, 1, 0.01).onChange(onAnimationChange);
                folderFrame.add(frame.motors, 'up', 0, 1, 0.01).onChange(onAnimationChange);
                folderFrame.add(frame.motors, 'down', 0, 1, 0.01).onChange(onAnimationChange);
            });
        }
    }

    function onAnimationChange() {
        setServerAnimations();
    }

    function onGuiChange() {
    }

    function onGuiSettingsChange() {
        setServerConfig();
    }

    var setAnimationsTimeout;
    function setServerAnimations() {
        clearTimeout(setAnimationsTimeout);
        setAnimationsTimeout = setTimeout(() => {
            socket.emit('setServerAnimations', animations);
        }, 100);
    }

    var setServerConfigTimeout;
    function setServerConfig() {
        clearTimeout(setServerConfigTimeout);
        setServerConfigTimeout = setTimeout(() => {
            socket.emit('setServerConfig', settings);
        }, 100);
    }


    var container = document.getElementById('canvasContainer');
    var canvas = document.createElement('canvas');
    container.appendChild(canvas);

    const degToRad = Math.PI / 180;
    const radToDeg = 180 / Math.PI;

    var ctx = canvas.getContext('2d');

    function redraw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.strokeStyle = 'red';
        ctx.fillStyle = 'red';
        ctx.lineWidth = 2;

        var circleSize = 50;
        var margin = 100;
        var space = 120;
        drawCircle(margin, margin + space, circleSize);
        ctx.stroke();
        drawCircle(margin, margin + space, circleSize * motorsState.left);
        ctx.fill();
        drawCircle(margin + space * 2, margin + space, circleSize);
        ctx.stroke();
        drawCircle(margin + space * 2, margin + space, circleSize * motorsState.right);
        ctx.fill();
        drawCircle(margin + space, margin + space, circleSize);
        ctx.stroke();
        drawCircle(margin + space, margin + space, circleSize * motorsState.center);
        ctx.fill();
        drawCircle(margin + space, margin, circleSize);
        ctx.stroke();
        drawCircle(margin + space, margin, circleSize * motorsState.up);
        ctx.fill();
        drawCircle(margin + space, margin + space * 2, circleSize);
        ctx.stroke();
        drawCircle(margin + space, margin + space * 2, circleSize * motorsState.down);
        ctx.fill();
        requestAnimationFrame(redraw);
    }

    requestAnimationFrame(redraw);

    function onResize() {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    onResize();

    function drawCircle(x, y, radius, startAngle, endAngle) {
        ctx.beginPath();
        if (startAngle !== undefined && endAngle !== undefined) {
            ctx.arc(x, y, radius, startAngle * degToRad, endAngle * degToRad);
        } else {
            ctx.arc(x, y, radius, 0, 2 * Math.PI);
        }
    }
}
